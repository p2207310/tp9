#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <algorithm>
#include <string>
#include <cassert>
#include <cmath>
#include "imagegraph.h"

using namespace std;

Pixel::Pixel() {
    intensity=0;
}

Pixel* ImageGraph::getPixel(int i, int j) {
    return &pixelArray[i*width+j];
}

Pixel* ImageGraph::getPixel(int i) {
    return &pixelArray[i];
}


int ImageGraph::getVoisinNord(int i) {
    if (i<width) {
        return -1;
    }
    return i-width;
}

int ImageGraph::getVoisinSud(int i) {
    if (i>(height-1)*width-1) {
        return -1;
    }
    return i+width;
}

int ImageGraph::getVoisinEst(int i) {
    if ((i+1)%width==0) {
        return -1;
    }
    return i+1;
}

int ImageGraph::getVoisinOuest(int i) {
    if (i%width==0) {
        return -1;
    }
    return i-1;                                     
}

//Charge les valeurs d'un fichier dans un tableau
void ImageGraph::loadImage(const char* fileName) {
    ifstream file(fileName);

    if (!file.is_open()) {
        cerr << "Erreur lors de l'ouverture du fichier." << endl;
        return;
    }
    string str;
    int temp;
    getline(file, str); getline(file, str);
    file >> width; file >> height; file >> maxIntensity;
    pixelArray.resize(height*width);
    while(!file.eof()){
        for(int lineHeight=0;lineHeight<height;lineHeight++) {
            for (int lineWidth=0;lineWidth<width;lineWidth++) {
                file>>temp;
                pixelArray[lineHeight*width+lineWidth].intensity=temp;
                
            }        
        }
    }
}

void ImageGraph::printImage(){
    for(int i=0; i < height; i++){
        for(int j=0; j<width; j++){
            string sep = "";
            int intensity = getPixel(i,j)->intensity;
            if(intensity<10){
                sep="   ";
            }
            else if(intensity>10 && intensity<100){
                sep="  ";
            }
            else{
                sep=" ";
            }
            cout << intensity <<sep;
        }
        cout<<endl;
    }

}

void ImageGraph::printVector(vector<int> v)  {
    for (auto& val : v){
        cout<<val<<" ";
    }
    cout<<endl;
}

vector<int> ImageGraph::BFS(int source, int destination) {
    int size=width*height;
    vector<int> precedent(size,-1);
    vector<bool> visited(size,false);
    queue<int> q;
    q.push(source);
    int temp,voisin;

    while (!q.empty()) {
        temp=q.front();
        q.pop();
        if (temp==destination) { break; }
        visited[temp]=true;

        voisin=getVoisinNord(temp);
        if (voisin!=-1 && visited[voisin]==false) {
            precedent[voisin]=temp;
            //if (pixelArray[temp]->flot<pixelArray[temp]->capacité)
            q.push(voisin);
        }
        voisin=getVoisinSud(temp);
        if (voisin!=-1 && visited[voisin]==false) {
            precedent[voisin]=temp;
            q.push(voisin);
        }
        voisin=getVoisinEst(temp);
        if (voisin!=-1 && visited[voisin]==false) {
            precedent[voisin]=temp;
            q.push(voisin);
        }
        voisin=getVoisinOuest(temp);
        if (voisin!=-1 && visited[voisin]==false) {
            precedent[voisin]=temp;
            q.push(voisin);
        }
    }
    vector<int> chemin;
    temp=destination;
    while (temp!=-1) {
        chemin.push_back(temp);
        temp=precedent[temp];
    }
    reverse(chemin.begin(),chemin.end());

    return chemin;
}
void ImageGraph::show_path(int src, int dest) {
    vector<int> path=BFS(src, dest);
    int curr = 0;
     for(int i=0; i < height; i++){
        for(int j=0; j<width; j++){
            
            string sep = "";
            int intensity = getPixel(i,j)->intensity;
            if(intensity<10){
                sep="   ";
            }
            else if(intensity>10 && intensity<100){
                sep="  ";
            }
            else{
                sep=" ";
            }
            if (path[curr]==i*width+j){
                sep="";
                
                if((path[curr+1])-path[curr]>=width){
                    cout <<"|   "<<sep;
                }
                else if(path[curr]==dest){
                    cout <<"v   " << sep;
                }
                else {
                    cout <<"‾‾‾‾"<<sep;
                }               
                
                curr++;
            }
            else{
                cout << intensity <<sep;
            }
            
        }
        cout<<endl;
    }

}

float ImageGraph::getIntensity(Pixel* p, Pixel* q)
{
    return exp(-(pow(p->intensity - q->intensity, 2)/2 * pow(sigma, 2)));
}

float Pixel::getCapSrc(int alpha)
{
    float a = (255 - (float)intensity) / 255;
    a = -(alpha * log(a));
    return a;
}

float Pixel::getCapPuit(int alpha)
{
    float a = (float)intensity/ 255;
    a = -(alpha * log(a));
    return a;
}

//le chemin est reverse, donc le dernier indice du tableau est la destination
int ImageGraph::getCapMin(vector<int> chemin) {
    int min=getPixel(chemin[0])->capSrc;
    int cap;
    Pixel* pix;
    for (int i=1;i<chemin.size()-1;i++) {
        if (chemin[i+1]==getVoisinNord(i)) {
            pix=getPixel(i);
            cap=pix->capNord-pix->flotNord+getPixel(chemin[i+1])->flotSud;
        } else if (chemin[i+1]==getVoisinSud(i)) {
            pix=getPixel(i);
            cap=pix->capSud-pix->flotSud+getPixel(chemin[i+1])->flotNord;
        } else if (chemin[i+1]==getVoisinOuest(i)) {
            pix=getPixel(i);
            cap=pix->capOuest-pix->flotOuest+getPixel(chemin[i+1])->flotEst;
        } else if (chemin[i+1]==getVoisinEst(i)) {
            pix=getPixel(i);
            cap=pix->capEst-pix->flotEst+getPixel(chemin[i+1])->flotSud;
        }
        if (min>cap) {
            min=cap;
        }
    }
    if (min > getPixel(chemin[chemin.size()-1])->capPuit){
        min=getPixel(chemin[chemin.size()-1])->capPuit;
    }
    return min;
}
