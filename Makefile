# Makefile

CC = g++
CFLAGS = -std=c++11 -Wall

all: main

main: main.o imagegraph.o
	$(CC) $(CFLAGS) -o main main.o imagegraph.o

main.o: main.cpp imagegraph.h
	$(CC) $(CFLAGS) -c main.cpp

imagegraph.o: imagegraph.cpp imagegraph.h
	$(CC) $(CFLAGS) -c imagegraph.cpp

clean:
	rm -f main main.o imagegraph.o
