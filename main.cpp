// main.cpp
#include "imagegraph.h"
#include <iostream>
#include <vector>
using namespace std;
  

int main() {
    ImageGraph graph;
    graph.loadImage("./image.txt");
    //graph.printImage();
    
    int src = 0; 
    int dest = 51;

    vector<int> chemin;
    chemin=graph.BFS(src,dest);
    graph.show_path(src,dest);
    graph.printVector(chemin);
    return 0;
}
