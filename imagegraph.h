// imagegraph.h
#ifndef IMAGEGRAPH_H
#define IMAGEGRAPH_H
#include <iostream>
#include <fstream>   
#include <vector>
#include <string>
using namespace std;

class Pixel {
    public:
        int intensity;
        int flotSrc,flotPuit,flotNord,flotSud,flotEst,flotOuest;
        int capSrc,capPuit,capNord, capSud, capEst, capOuest;
        Pixel();
        float getCapSrc(int alpha);
        float getCapPuit(int alpha);
};


class ImageGraph {
    public:
        vector<Pixel> pixelArray;
        int width, height, maxIntensity;
        float alpha,sigma;
        void loadImage(const char* fileName);
        void printImage();
        void printVector(vector<int> v);
        Pixel* getPixel(int i, int j);
        Pixel* getPixel(int i);
        int getVoisinNord(int i);
        int getVoisinSud(int i);
        int getVoisinEst(int i);
        int getVoisinOuest(int i);
        vector<int> BFS(int source, int destination);
        void show_path(int src, int dest);
        float getIntensity(Pixel* p, Pixel* q);

        int getCapMin(vector<int> chemin);
};


#endif // IMAGEGRAPH_H
